import { Injectable } from '@angular/core';
import { Adventure } from '../adventure/adventure/adventure';
import { HttpClient, HttpParams } from '@angular/common/http'
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

//const apiUrl = 'http://localhost:8085/';
const apiUrl = 'http://192.168.99.100:8080/adventure/';
const adventureMSUrl = 'api/v1/ms/adventure/';


@Injectable({
  providedIn: 'root'
})
export class AdventureService {

  constructor(private http: HttpClient) { }

  getAdventures(pageNg,nbPerPage) : Observable<Adventure[]>{
    
    let params = new HttpParams()
    .set('pageNg', pageNg)
    .set('nbPerPage', nbPerPage);
    
    
    return this.http.get<Adventure[]>(apiUrl + adventureMSUrl, {params: params})
    .pipe(
      tap(_ => this.log('fetched Adventures')),
      catchError(this.handleError('getAdventures', []))
      );
  }

  getAdventureById(id: number): Observable<Adventure> {
    return this.http.get<Adventure>(apiUrl + adventureMSUrl+id)
    
  }
  private handleError<T>(operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string){
    console.log(message);
  }
}
