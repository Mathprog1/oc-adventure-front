import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Category } from '../category/category/category';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Adventure } from '../adventure/adventure/adventure';
import { Router } from '@angular/router';

//const apiUrl = 'http://localhost:8085/';
const apiUrl = 'http://192.168.99.100:8080/adventure/';
const categoryMSUrl = 'api/v1/ms/category/';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient, private router: Router) { }

  getCategories(pageNg,nbPerPage) : Observable<Category[]>{
    
    let params = new HttpParams()
    .set('pageNg', pageNg)
    .set('nbPerPage', nbPerPage);
    
    
    return this.http.get<Category[]>(apiUrl + categoryMSUrl, {params: params})
    .pipe(
      tap(_ => this.log('fetched Categories')),
      catchError(this.handleError('getCategories', []))
      );
  }

  getCategoryById(id: number): Observable<Category> {

    return this.http.get<Category>(apiUrl + categoryMSUrl+id)
    
  }

  getCategoryByTypeAdventure( typeAdventure: string): Observable<Category> {
    return this.http.get<Category>(apiUrl + categoryMSUrl+ "type/" +typeAdventure)
}

  private handleError<T>(operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string){
    console.log(message);
  }
}
