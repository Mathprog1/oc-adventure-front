import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Comment } from '../comment/comment';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

const apiUrl = 'http://192.168.99.100:8080/comment/';
const commentMSUrl = 'api/v1/ms/comment/';


@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient,private authService: AuthService,private router: Router) { }

  getComments(pageNb,nbPerPage) : Observable<Comment[]>{
    
    let params = new HttpParams()
    .set('pageNb', pageNb)
    .set('nbPerPage', nbPerPage);
    console.log(apiUrl + commentMSUrl, {params: params});
    return this.http.get<Comment[]>(apiUrl + commentMSUrl + 'adventure/' , {params: params})
    .pipe(
      tap(_ => this.log('fetched Comments')),
      catchError(this.handleError('getComments', []))
      );
  }

  getCommentsByAdventureId(externalId,pageNb,nbPerPage) : Observable<Comment[]>{
    
    let params = new HttpParams()
    .set('pageNb', pageNb)
    .set('nbPerPage', nbPerPage);
    console.log(apiUrl + commentMSUrl + externalId + '/', {params: params});
    return this.http.get<Comment[]>(apiUrl + commentMSUrl + 'adventure/' + externalId + '/', {params: params})
    .pipe(
      tap(_ => this.log('fetched Comments')),
      catchError(this.handleError('getComments', []))
      );
  }

  saveCommentByAdventureId(form: any): Observable<any>  {

    console.log('new com service',form);
    return this.http.post<any> (apiUrl + commentMSUrl + 'register/', form)
    .pipe(
      tap(_ => this.log('save comment')),
      catchError(this.handleError('save comment', []))
    );
  }

  private handleError<T>(operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string){
    console.log(message);
  }
}
