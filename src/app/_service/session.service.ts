import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Session } from '../session/session/session';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

const apiUrl = 'http://192.168.99.100:8080/session/';
const sessionMSUrl = 'api/v1/ms/session/adventure/';


@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private http: HttpClient) { }

  getSessions(pageNb,nbPerPage) : Observable<Session[]>{
    
    let params = new HttpParams()
    .set('pageNb', pageNb)
    .set('nbPerPage', nbPerPage);
    
    return this.http.get<Session[]>(apiUrl + sessionMSUrl, {params: params})
    .pipe(
      tap(_ => this.log('fetched Sessions')),
      catchError(this.handleError('getSessions', []))
      );
  }

  getSessionsByAdventureId(externalId,pageNb,nbPerPage) : Observable<Session[]>{
    
    let params = new HttpParams()
    .set('pageNb', pageNb)
    .set('nbPerPage', nbPerPage);
    
    return this.http.get<Session[]>(apiUrl + sessionMSUrl + externalId + '/', {params: params})
    .pipe(
      tap(_ => this.log('fetched Sessions')),
      catchError(this.handleError('getSessions', []))
      );
  }

  private handleError<T>(operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string){
    console.log(message);
  }
}
