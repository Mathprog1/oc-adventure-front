import { Component, OnInit, Input, Injectable } from '@angular/core';
import { Adventure } from '../adventure/adventure';
import { AdventureService } from '../../_service/adventure.service';
import { Router, NavigationExtras } from '@angular/router';
import { FormGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'app-adventure-detail',
  templateUrl: './adventure-detail.component.html',
  styleUrls: ['./adventure-detail.component.scss']
})
export class AdventureDetailComponent implements OnInit {

  commentForm: FormGroup;
  isLoadingResults = false;


  @Input()a: Adventure; ;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onClick(a: Adventure): void {

    let navigationExtras: NavigationExtras = {
      queryParams: {
       "externalId" : a.adventureId,
       "typeAdventure" : a.nameAdventure
      }
    };
    this.router.navigate(["session",a], navigationExtras);
    }



    onFormSubmit(form: NgForm) {
      // stop here if form is invalid
      this.isLoadingResults = true;
      if (this.commentForm.invalid) {
        return false;
      }
      /*
this.commentService.registerComment(form)
      */
    }
}
