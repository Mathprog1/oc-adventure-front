import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdventureInfoComponent } from './adventure-info.component';

describe('AdventureInfoComponent', () => {
  let component: AdventureInfoComponent;
  let fixture: ComponentFixture<AdventureInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdventureInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdventureInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
