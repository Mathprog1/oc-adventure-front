import { Component, OnInit, Injectable } from '@angular/core';
import { Router,ActivatedRoute, NavigationExtras } from '@angular/router';
import { AdventureService } from 'src/app/_service/adventure.service';
import { Adventure } from '../adventure/adventure';
import { Observable } from 'rx';
import { Category } from 'src/app/category/category/category';
import { AdventureComponent } from '../adventure/adventure.component';
import { FormGroup, FormBuilder, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import {Comment} from 'src/app/comment/comment';
import { CommentService } from 'src/app/_service/comment.service';
import { AuthService } from '../../auth.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-adventure-info',
  templateUrl: './adventure-info.component.html',
  styleUrls: ['./adventure-info.component.scss']
})

export class AdventureInfoComponent implements OnInit {

selectedAdventure : Adventure;
selectAdventure = false;
commentsList : Comment[] = [];
categoryList : Category[];
adventures : Adventure[]
adventureId: number;
adventure: Adventure;
nameAdventure: string;
isLoadingResults = true;
externalId: number;
private sub : any;
commentForm : FormGroup;
pageNg = 1;
nbPerPage = 30;

/*commentsList : Comment[]*/ 
  constructor(private formBuilder: FormBuilder,private adv: AdventureComponent , private router: Router,private route: ActivatedRoute, 
    private adventureService: AdventureService, private commentService: CommentService, public authService: AuthService, public datePipe: DatePipe) { }

  ngOnInit() {
     this.sub =    this.route.params.subscribe(params => {
      this.externalId = params['externalId'];
    });
    console.log('extId',this.externalId);
    console.log('page',this.pageNg,this.nbPerPage);
    this.getAdventureById();
    this.getCommentsByAdventureId();

    this.commentForm = this.formBuilder.group({
      comment: [null, Validators.required]
    });


    /*
    this.commentForm = this.formBuilder.group({
      userExternalId:[]
    });
    */

    /*
this.commentService.getComments().subscribe(res=> this)
    */
  }

  onClick(a: Adventure): void {

    let navigationExtras: NavigationExtras = {
      queryParams: {
       "externalId" : a.adventureId,
       "typeAdventure" : a.nameAdventure
      }
    };
    this.router.navigate(["session",a], navigationExtras);
    }

getAdventureById(): void {
  this.adventureService.getAdventureById(this.externalId)
  .subscribe(adventure => {
    this.adventure = adventure;
    console.log("SELECTED ADVENTURE --->" + this.adventure.adventureId);
    this.isLoadingResults=false;
  }, err => {
    console.log(err),
    this.isLoadingResults = false;
  })
}

getCommentsByAdventureId(): void {
  console.log('page',this.pageNg,this.nbPerPage);
  this.commentService.getCommentsByAdventureId(this.externalId,this.pageNg,this.nbPerPage)
  .subscribe(comments => {
    this.commentsList = comments;
    console.log('data',this.commentsList);
    this.isLoadingResults = false;
  }, err => {
    console.log(err),
    this.isLoadingResults = false;
  })
}

saveCommentByAdventureId(data:any): void {
  this.commentService.saveCommentByAdventureId(data)
  .subscribe(comments => {
    this.commentsList = comments;
    console.log('data',this.commentsList);
    this.getCommentsByAdventureId();
    this.isLoadingResults = false;
  }, err => {
    console.log(err),
    this.isLoadingResults = false;
  })  
}

onFormSubmit(form: NgForm) {
  // stop here if form is invalid
  this.isLoadingResults = true;
  if (this.commentForm.invalid) {
    return false;
  }

  const decodeToken = this.authService.decodeToken();
    if (!decodeToken) {
      console.log('Invalide token !');
      this.router.navigate(['login']);
    }

  const data = {
    "commentInput": form["comment"],
	  "dateComment" : new Date(),
	  "adventureExternalId": this.externalId,
	  "userExternalId": decodeToken.externalId
  };

  console.log('form',form["comment"]);
  console.log('new com',data);

  this.saveCommentByAdventureId(data);
}

navigate(location: string) {
  this.router.navigate([location]);
}

}


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}