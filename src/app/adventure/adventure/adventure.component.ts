import { Component, OnInit, Injectable } from '@angular/core';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { AdventureService } from '../../_service/adventure.service';
import { Router, NavigationExtras } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material/core';
import { Adventure } from './adventure';


@Component({
  selector: 'app-adventure',
  templateUrl: './adventure.component.html',
  styleUrls: ['./adventure.component.scss']
})

@Injectable({
  providedIn: 'root'
})
export class AdventureComponent implements OnInit {
  data: Adventure[] = [];

  selectedAdventure : Adventure;
  pageNg = 1;
  nbPerPage = 30;
  displayedColumns: string[] = ['locationAdventure', 'descriptionAdventure', 'totalVoters','totalVoting','adventureRating']
  isLoadingResults = true;
  selectAdventure = false;
  

  constructor(private router: Router, private adventureService: AdventureService) { }
 

  getAdventures(): void {
    this.adventureService.getAdventures(this.pageNg,this.nbPerPage)
    .subscribe(adventures => {
      this.data = adventures;
      console.log(this.data);
      this.isLoadingResults = false;
    }, err => {
      console.log(err),
      this.isLoadingResults = false;
    })
  }

ngOnInit() {
  this.getAdventures();
}
getAdventureById(id: number): Adventure {
    return this.data.filter(adventure => adventure.adventureId === id)[0];
}

onSelect(a: Adventure):void {
  if(this.selectAdventure===false) {
    this.selectedAdventure = a;
    this.selectAdventure = true;
  }else{
    this.selectedAdventure = null;
    this.selectAdventure = false;
  }
}

onClick(a: Adventure): void {

  let navigationExtras: NavigationExtras = {
    queryParams: {
     "externalId" : a.adventureId,
     "nameAdventure" : a.nameAdventure
    }
  };
  this.router.navigate(["adventure/info",a], navigationExtras);
  }

}
