import { Category } from 'src/app/category/category/category';

export class Adventure {
    adventureId: number;
    nameAdventure: string;
    locationAdventure: string;
    descriptionAdventure : string;
    totalVoters: number;
    totalVoting: number;
    adventureRating: number;
    categoryList: Category[];   

    
}