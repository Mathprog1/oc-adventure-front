import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { AuthService } from './auth.service';
import { ProfilComponent } from './profil/profil.component';
import { ValidationComponent } from './auth/validation/validation.component';
import { EditComponent } from './profil/edit/edit.component';
import { AddressComponent } from './profil/address/address.component';
import { AddressEditComponent } from './profil/address/address.edit/address.edit.component';
import { AdventureComponent } from './adventure/adventure/adventure.component';
import { CategoryComponent } from './category/category/category.component';
import { CartComponent } from './cart/cart.component';
import { SessionComponent } from './session/session/session.component';
import { CategoryDetailComponent } from './category/category-detail/category-detail.component';
import { CategorySearchComponent } from './category/category-search/category-search.component';
import { AdventureInfoComponent } from './adventure/adventure-info/adventure-info.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'adventure',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component : LoginComponent,
    data: { title : 'Login'}
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: { title : 'Register'}
  },
  {
    path: 'validate',
    component: ValidationComponent,
    data: { title : 'Account Validation'}
  },
  {
    path: 'cart',
    component: CartComponent,
    data: { title : 'Your Cart'}
  },
  {
    path: 'profile',
    component: ProfilComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: { title : 'Profile',
            allowedRoles: ['USER_STANDARD']
          }
  },
  {
    path: 'profile/edit',
      component: EditComponent,
      canActivate: [AuthGuard],
      canActivateChild: [AuthGuard],
      data: { title : 'Profile',
            allowedRoles: ['USER_STANDARD']
          }
  },
  {
    path: 'profile/address',
      component: AddressComponent,
      canActivate: [AuthGuard],
      canActivateChild: [AuthGuard],
      data: { title : 'Register your address',
            allowedRoles: ['USER_STANDARD']
          }
  }
  // address/edit/
  ,
  {
    path: 'profile/address/edit/:externalId',
      component: AddressEditComponent,
      canActivate: [AuthGuard],
      canActivateChild: [AuthGuard],
      data: { title : 'Register your address',
            allowedRoles: ['USER_STANDARD']
          }
  }
  ,
  {
    path: 'adventure',
    component: AdventureComponent,
    data: { title : 'Adventure'}
  },
  {
    path: 'category',
    component: CategoryComponent,
    data: { title : 'Category'}
  },
  {
    path: 'session',
    component: SessionComponent,
    data: { title : 'Session'}
  },
  {
    path: 'category/search',
    component: CategoryDetailComponent,
    data: { title : 'CategoryDetail'}
  },
  {
    path: 'category/search/result',
    component: CategorySearchComponent,
    data: { title : 'CategorySearch'}
  },
  {
    path: 'adventure/info',
    component: AdventureInfoComponent,
    data: { title : 'Adventure'}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
