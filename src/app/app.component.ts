import { Component } from '@angular/core';
import { MatMenuTrigger} from '@angular/material';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';

  constructor(public authService: AuthService) {

  }

}
