import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatListModule,
  MatMenuModule,
  MatSelectModule,
  MatGridListModule,
  MatSidenavModule,
  MatToolbarModule,
  MatStepperModule,
  MatAutocompleteModule,
  MatDatepickerModule,
   MatNativeDateModule,
   MatTabsModule,
   MatRadioModule
} from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
import {ShoppingCartModule} from 'ng-shopping-cart'; // <-- Import the module class
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import { JwtModule, JwtModuleOptions } from '@auth0/angular-jwt';
import { AuthService } from './auth.service';
import { ProfilComponent } from './profil/profil.component';
import { ValidationComponent } from './auth/validation/validation.component';
import { EditComponent } from './profil/edit/edit.component';
import { AddressEditComponent } from './profil/address/address.edit/address.edit.component';
import { AddressComponent } from './profil/address/address.component';
import { CategoryService } from './_service/category.service';
import { CategoryDetailComponent } from './category/category-detail/category-detail.component';
import { AdventureService } from './_service/adventure.service';
import { AdventureDetailComponent } from './adventure/adventure-detail/adventure-detail.component';
import { AdventureComponent } from './adventure/adventure/adventure.component';
import { CategoryComponent } from './category/category/category.component';
import { SessionComponent } from './session/session/session.component';
import { CategorySearchComponent } from './category/category-search/category-search.component';
import { CartComponent } from './cart/cart.component';
import { SessionCartItem } from './cart/SessionCartItem';
import { CartViewComponent } from './cart/perso-cart-view.component';
import { AdventureInfoComponent } from './adventure/adventure-info/adventure-info.component';
import { DatePipe } from '@angular/common'

export function getToken() {
  return localStorage.getItem('token');
 }

// tslint:disable-next-line:variable-name
const JWT_Module_Options: JwtModuleOptions = {
  config: {
      tokenGetter: getToken,
      whitelistedDomains: ['localhost:8080']
  }
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ProfilComponent,
    ValidationComponent,
    EditComponent,
    AddressEditComponent,
    AddressComponent,
    AdventureComponent,
    CategoryComponent,
    AdventureDetailComponent,
    CategoryDetailComponent,
    CategorySearchComponent,
    SessionComponent,
    CartComponent,
    CartViewComponent,
    AdventureInfoComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    MatListModule,
    MatSelectModule,
    MatFormFieldModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    FlexLayoutModule,
    JwtModule.forRoot(JWT_Module_Options),
    MatTabsModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatStepperModule,
    MatRadioModule,
    MatExpansionModule,
    ShoppingCartModule.forRoot({ // <-- Add the cart module to your root module
      itemType: SessionCartItem, // <-- Configuration is optional
      serviceType: 'localStorage',
      serviceOptions: {
        storageKey: 'NgShoppingCart',
        clearOnError: true
      }
    })  ],
  exports: [
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true,
    useValue: localStorage    
  },
    {
      provide: AuthService,
      useClass: AuthService
    },
    DatePipe
],
  bootstrap: [AppComponent]
})
export class AppModule { }
