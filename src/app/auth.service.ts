import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

const apiUrl = 'http://192.168.99.100:8080/';
const authUrl = 'auth/api/v1/ms/auth/';
const userMSUrl = 'user/api/v1/ms/user/';
const consistencyMSUrl = 'consistency/api/v1/ms/consistency/user/';
export const TOKEN_NAME = 'token';


@Injectable({
  providedIn: 'root'
})
export class AuthService {


  isLoggedIn = false;
  redirectUrl: string;

  constructor(private http: HttpClient, private jwtHelperService: JwtHelperService, private router: Router) { }

  existsMail(mail: string): Observable<any> {
    return this.http.get<any>(apiUrl + userMSUrl + 'exists/mail/' + mail)
    .pipe(
      tap(),
      catchError(this.handleError('exists Mail test', []))
    );
  }

  existsPhone(phoneNumber: string): Observable<any> {
    return this.http.get<any>(apiUrl + userMSUrl + 'exists/phone/' + phoneNumber)
    .pipe(
      tap(),
      catchError(this.handleError('exists Phone test', []))
    );
  }

  login(data: any): Observable<any> {
    return this.http.post<any>(apiUrl + authUrl + 'login/', data)
    .pipe(
      tap(_ => this.isLoggedIn = true),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
            this.router.navigate(['login'], { queryParams: { error: true } });
        }
        if (error.status === 400) {
            alert(error.error);
        }
        return throwError(error);
    }
    ));
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['login']);
  }

  isLogged(): boolean {
    return !this.isTokenExpired();
  }

  register(data: any): Observable<any>  {
    return this.http.post<any> (apiUrl + consistencyMSUrl + 'register/', data)
    .pipe(
      tap(_ => this.log('register')),
      catchError(this.handleError('register', []))
    );
  }



  validate(externalId: string, validationToken: string): Observable<any> {
    return this.http.get<any> (apiUrl + userMSUrl + 'validate/?ValidationToken=' + validationToken + '&externalId=' + externalId)
    .pipe(
      tap(_ => this.log('validate')),
      catchError(this.handleError('validate', []))
    );
  }

  existing(controlName: string, url: string): Observable<any> {
    return this.http.get<any> (apiUrl + userMSUrl + url + controlName)
    .pipe(
      tap(_ => this.log('existingEmail')),
      catchError(this.handleError('existingEmail', []))
    );
}


  setToken(token: string) {
    localStorage.setItem(TOKEN_NAME, token);
  }

  isAuthorized(allowedRoles: string[]): boolean {
    if (allowedRoles == null || allowedRoles.length === 0) {
      return true;
    }
    // console.log(allowedRoles);
    const token = localStorage.getItem('token');
    const decodeToken = this.jwtHelperService.decodeToken(token);
    // console.log(decodeToken);
    if (!decodeToken) {
      console.log('Invalide token !');
      return false;
    }

    for ( const role of decodeToken.roles ) {
      // console.log(role);
      // console.log(allowedRoles);
      if (allowedRoles.includes(role)) {
        return true;
      }
    }

    return false;

  }

  getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }

  decodeToken(): any {
    return this.jwtHelperService.decodeToken(this.getToken());
  }

  getTokenExpiration(token: string): Date {
      const decoded = this.jwtHelperService.decodeToken(token);
      // console.log(decoded);
      if ( decoded.exp === undefined) { return null; }

      const date = new Date(0);
      date.setUTCSeconds(decoded.exp);
      return date;
  }

  isTokenExpired(token?: string): boolean {
    if (!token) { token = this.getToken(); }
    if (!token) { return true; }

    const date = this.getTokenExpiration(token);
    if (date === undefined) { return false; }
    return !(date.valueOf() > new Date().valueOf());
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to the console
      this.login(`${operation} failed : ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }
}
