import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      const allowedRoles = next.data.allowedRoles;
      const isAuthorized = this.authService.isAuthorized(allowedRoles);

      if(!isAuthorized){
        console.log('pas authorisé !');
        this.router.navigate(['login']);
      }
      const url: string = state.url;
      return this.checkLogin(url) && isAuthorized;
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      const allowedRoles = next.data.allowedRoles;
      const isAuthorized = this.authService.isAuthorized(allowedRoles);

      if(!isAuthorized){
        console.log('pas authorisé !');
        this.router.navigate(['login']);
      }
      const url: string = state.url;
      return this.checkLogin(url) && isAuthorized;
  }


  checkLogin(url: string): boolean {
    console.log('expired? ' + this.authService.isTokenExpired());
    if (!this.authService.isTokenExpired()) {
      return true;
    }

    this.authService.redirectUrl = url;
    this.router.navigate(['login']);
    return false;
  }

}
