import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { AuthService } from '../../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  error = false;
  mail = '';
  password = '';
  matcher = new MyErrorStateMatcher();
  isLoadingResults = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute,
              private authService: AuthService) {
                this.route
                .queryParams
                .subscribe(params => {
                  this.error = params.error || false;
                  if (this.error) {
                    this.isLoadingResults = false;
                  }
                });
               }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      mail: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.authService.login(form).subscribe(
      res => {
        if (res.token) {
          this.authService.setToken(res.token);
          this.error = false;
          this.router.navigate(['profile']);
        }
      }, (err) => {
        this.isLoadingResults = false;
        console.log(err);
      });
  }

  register() {
    this.router.navigate(['register']);
  }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
