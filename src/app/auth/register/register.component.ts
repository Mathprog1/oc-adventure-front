import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material/core';
import { MustMatch } from '../../_helper/must-match.validator';
import { CustomUniqueValidator } from 'src/app/_helper/CustomUniqueValidator.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  isLoadingResults = false;
  matcher = new MyErrorStateMatcher();

  constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthService,
              private uniqueValidator: CustomUniqueValidator) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: [null, Validators.required],
      mail: [null, [Validators.required, Validators.email],
      [this.uniqueValidator.existingUniqueValidator('exists/mail/')]],
      password: [null, Validators.required],
      confirmPassword: ['', Validators.required],
      lastName: [null, Validators.required],
      phoneNumber: [null, [Validators.required],
      [this.uniqueValidator.existingUniqueValidator('exists/phone/')]],
    }, {
      validators: MustMatch('password', 'confirmPassword')
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onFormSubmit(form: NgForm) {
    // stop here if form is invalid
    this.isLoadingResults = true;
    if (this.registerForm.invalid) {
      return false;
    }

    this.authService.register(form)
      .subscribe(res => {
        this.router.navigate(['login']);
      }, (err) => {
        this.isLoadingResults = false;
        console.log(err);
        alert(err);
      });
  }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
