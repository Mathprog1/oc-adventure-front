import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})
export class ValidationComponent implements OnInit {

  externalId: string;
  validationToken: string;
  isError: boolean;
  isLoading = true;

  constructor(private authService: AuthService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.externalId = params.externalId;
      this.validationToken = params.ValidationToken;
  });
    this.validate();
  }

  validate(): void {
    this.authService.validate(this.externalId, this.validationToken).subscribe((isError: { exists: boolean; }) => {
      this.isError = isError.exists;
      console.log(this.isError);
      this.isLoading = false;
    }, (err: any) => {
      console.log(err);
      this.isLoading = false;
    });
  }

}
