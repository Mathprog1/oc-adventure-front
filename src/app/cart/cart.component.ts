import { Component, OnInit } from '@angular/core';
import { CartService } from 'ng-shopping-cart';
import { SessionCartItem } from './SessionCartItem';
import { AuthService } from '../auth.service';
import { ProfilService } from '../profil.service';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { Address } from '../profil/entity/address';
import { Commande } from './commande';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  addressList: Address[];
  choosedAdress: Address;
  publicKey = 'pk_test_NEYItSoeGhhUo4V3JqCIs8x0005MaBKQo1';

  headers = {
    empty: 'No items. Add some to the cart',
    name: 'Description',
    quantity: 'Quantity',
    price: 'Cost',
    total: 'Total',
    startDate: 'Starting date',
    endDate: 'Ending Date'
  };

  footers = {
    tax: 'Tax rate',
    shipping: 'Shipping cost',
    total: 'Total cost'
  };

  constructor(public cartService: CartService<SessionCartItem>, public authService: AuthService,
              public profilService: ProfilService) { }

  ngOnInit() {

    if (this.authService.isLogged()) {
      this.getFacturationAddress();
    }

    this.cartService.setLocaleFormat('€ ');

    // console.log(this.cartService.getItems());
    this.loadStripe();
  }

  loadStripe() {
    if (!window.document.getElementById('stripe-script')) {
      const s = window.document.createElement('script');
      s.id = 'stripe-script';
      s.type = 'text/javascript';
      s.src = 'https://checkout.stripe.com/checkout.js';
      window.document.body.appendChild(s);
    }
}

pay(profilAction: ProfilService) {
  const totalCost = this.cartService.totalCost() * 100;
  const items = this.cartService.getItems();
  // tslint:disable-next-line:max-line-length
  const adresse: string = this.choosedAdress.address.concat( ', ' + this.choosedAdress.postalCode + ' ' + this.choosedAdress.city + ', ' + this.choosedAdress.country);
  this.cartService.clear();
  const extId = this.authService.decodeToken().externalId;
  const handler = (window as any).StripeCheckout.configure({
    key: 'pk_test_NEYItSoeGhhUo4V3JqCIs8x0005MaBKQo1',
    locale: 'auto',
    token(token: any) {
      // You can access the token ID with `token.id`.
      // Get the token ID to your server-side code for use.
      console.log(token);
      // Now we send the informations to the back
      // tslint:disable-next-line:max-line-length
      const commande = new Commande(totalCost / 100, token.email, token.id, 'EUR', adresse, items, extId);
      profilAction.createCommande(commande).subscribe();
      console.log(commande);
    }
  });
  handler.open({
    name: 'Wild Adventure',
    description: 'Sessions paiement',
    amount: totalCost,
    currency: 'EUR',
    zipcode: 'false',
    image: 'https://warnercnr.colostate.edu/wp-content/uploads/sites/2/2017/04/shutterstock_428626417-1024x683.jpg'
  });
}

  getFacturationAddress(): void {
    this.profilService.getUserFacturationAddress().subscribe(res => {
      this.addressList = res.addressList;
      console.log(this.addressList);
    }, err => {
      console.log(err);
    }
    );
  }
}
