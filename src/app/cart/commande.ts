import { SessionCartItem } from './SessionCartItem';

export class Commande {
    constructor(totalCost: number, stripeEmail: string, stripeToken: string, currency: string, fullAddress: string,
                commandeSessionInDtoList: SessionCartItem[], userExternalId: string  ) {
         this.totalCost = totalCost;
         this.stripeEmail = stripeEmail;
         this.stripeToken = stripeToken;
         this.currency = currency;
         this.fullAddress = fullAddress;
         this.commandeSessionInDtoList = commandeSessionInDtoList;
         this.userExternalId = userExternalId;
    }

    private totalCost: number;

    private stripeEmail: string;

    private stripeToken: string;

    private currency: string;

    private fullAddress: string;

    private commandeSessionInDtoList: SessionCartItem[];

    private userExternalId: string;

}
