import { CartItem } from 'ng-shopping-cart';

export class SessionCartItem extends CartItem {

    constructor(itemData: any) {
        super();
        this.sessionExternalId = itemData.externalId;
        this.name = itemData.name;
        this.price = itemData.price;
        this.quantity = itemData.quantity;
        this.image = itemData.image;
        this.startDate = itemData.startDate;
        this.endDate = itemData.endDate;
      }


    public sessionExternalId: string;
    public name: string;
    public price: number;
    public quantity: number;
    public image: string;
    public startDate: Date;
    public endDate: Date;

    getId(): string {
        return this.sessionExternalId;
    }

    getName(): string {
        return this.name;
    }
    getPrice(): number {
        return this.price;
    }
    setQuantity(quantity: number): void {
        this.quantity = quantity;
    }
    getQuantity(): number {
        return this.quantity;
    }
    getImage(): string {
        return this.image;
    }

    getStartDate(): Date {
        return this.startDate;
    }
    getEndDate(): Date {
        return this.endDate;
    }

}
