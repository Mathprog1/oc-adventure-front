import { Component, OnInit } from '@angular/core';
import { AdventureService } from 'src/app/_service/adventure.service';
import { CategoryService } from 'src/app/_service/category.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Category } from '../category/category';
import { FormControl, FormGroup, FormBuilder, NgForm } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.scss']
})
export class CategoryDetailComponent implements OnInit {
  data: Category[] = [];

  selectedCategory : Category;
  pageNg = 1;
  nbPerPage = 30;

  isLoadingResults = true;
  selectCategory = false;
  categoryId : number;

  selectCategoryForm : FormGroup;

  
  typeAdventure: string;

  filteredOptions: Observable<Category[]>;
  constructor(private formBuilder: FormBuilder, private categoryService: CategoryService, private adventureService: AdventureService, 
    private router: Router) {
    
 
  }
  ngOnInit() {
    this.getCategories();
    this.selectCategoryForm = new FormGroup({
      data: new FormControl()
   });
 //this.getSessions();
 
   }
 
  getCategories(): void {
    this.categoryService.getCategories(this.pageNg,this.nbPerPage)
    .subscribe(categories => {
      this.data = categories;
      console.log(this.data);
      this.isLoadingResults = false;
    }, err => {
      console.log(err),
      this.isLoadingResults = false;
    })
  }

  
  

onFormSubmit(form: NgForm) {
  this.isLoadingResults = true;

  
}
onSelect(c: Category): void {

let navigationExtras: NavigationExtras = {
  queryParams: {
   "externalId" : c.categoryId,
   "typeAdventure" : c.typeAdventure,
   "adventureList" : c.adventureList
  }
};
this.router.navigate(["category/search/result",c], navigationExtras);
}



}