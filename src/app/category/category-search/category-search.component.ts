import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { CategoryService } from 'src/app/_service/category.service';
import { AdventureService } from 'src/app/_service/adventure.service';
import { Category } from '../category/category';
import { CategoryDetailComponent } from '../category-detail/category-detail.component';
import { Adventure } from 'src/app/adventure/adventure/adventure';

@Component({
  selector: 'app-category-search',
  templateUrl: './category-search.component.html',
  styleUrls: ['./category-search.component.scss']
})
export class CategorySearchComponent implements OnInit {
  categoryId : number;
  typeAdventure : String;
  adventureList : Adventure[];
category : Category;
adventure: Adventure;
private sub : any;
isLoadingCategoryResults = true;
 
  constructor(private categoryService: CategoryService,private route: ActivatedRoute, private router: Router) { 

  }

  ngOnInit() {


this.sub = this.route.params.subscribe(params => {

  this.categoryId= +params ['externalId'];
  this.typeAdventure = params ['typeAdventure'];
  this.adventureList = params ['adventureList'];
});
this.categoryService.getCategoryById(this.categoryId);
this.getCategoryInfo();
  }
 
  

getCategoryInfo() {
this.categoryService.getCategoryById(this.categoryId).subscribe
(selectedCategory => {
this.category = selectedCategory;
console.log("SELECTED CATEGORY ---->" + this.category.typeAdventure);
this.isLoadingCategoryResults = false;
}, err => {
  console.log(err);
  this.isLoadingCategoryResults = false;
});
}
onClick(a: Adventure): void {

  let navigationExtras: NavigationExtras = {
    queryParams: {
     "externalId" : a.adventureId,
     "typeAdventure" : a.nameAdventure
    }
  };
  this.router.navigate(["session",a], navigationExtras);
  }

  onHover(a: Adventure): void {

    let navigationExtras: NavigationExtras = {
      queryParams: {
       "externalId" : a.adventureId,
       "nameAdventure" : a.nameAdventure
      }
    };
    this.router.navigate(["adventure/info",a], navigationExtras);
    }


}
