import { Component, OnInit } from '@angular/core';
import { Category } from './category';
import { Router } from '@angular/router';
import { CategoryService } from '../../_service/category.service';
import { Adventure } from 'src/app/adventure/adventure/adventure';
import { AdventureService } from 'src/app/_service/adventure.service';
import { AdventureComponent } from 'src/app/adventure/adventure/adventure.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

 
  data: Category[] = [];

  selectedCategory : Category;
  pageNg = 1;
  nbPerPage = 30;
  displayedColumns: string[] = ['typeAdventure']
  isLoadingResults = true;
  selectCategory = false;
  
  wordsToSearch: string;
  resultList: Category[]= [];

  constructor(private router: Router, private categoryService: CategoryService, private adventureService: AdventureService, private adventureComponent: AdventureComponent) { }
 

  getCategories(): void {
    this.categoryService.getCategories(this.pageNg,this.nbPerPage)
    .subscribe(categories => {
      this.data = categories;
      console.log(this.data);
      this.isLoadingResults = false;
    }, err => {
      console.log(err),
      this.isLoadingResults = false;
    })
  }

ngOnInit() {
  this.getCategories();
}
getCategoryById(id: number): Category {
    return this.data.filter(category => category.categoryId === id)[0];
}


getAdventureListByCategoryId(categoryId: number): Adventure[] {
  return this.data.filter(category => category.categoryId === categoryId)[0].adventureList;
}
searchAdventureByCategory() {

  if(this.wordsToSearch === '') {
this.data.forEach((category) => {
   category.adventureList;
 })
} else {
  this.resultList = this.data.filter(
    c => c.typeAdventure.indexOf(this.wordsToSearch.toUpperCase()) !== -1
  );
}
}

onSelect(a: Category):void {
  if(this.selectCategory===false) {
    this.selectedCategory = a;
    this.selectCategory = true;
  }else{
    this.selectedCategory = null;
    this.selectCategory = false;
  }
}
}


