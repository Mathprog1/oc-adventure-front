import { Adventure } from 'src/app/adventure/adventure/adventure';

export class Category {
    categoryId: number;
    typeAdventure: string;
    adventureList: Adventure[];

    
}