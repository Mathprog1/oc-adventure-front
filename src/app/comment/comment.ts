import { User } from '../profil/entity/user';

export class Comment {
    commentId : number;
    adventureId: number;
    userId: string;
    commentInput: string;
    dateComment : Date;
    userOutDto : User; 
}