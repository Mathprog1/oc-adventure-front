import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private router: Router, private authService: AuthService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = localStorage.getItem('token');

        const isValide = this.authService.isTokenExpired(token);
        console.log(isValide);
        // console.log(request);

        if (!isValide) {
            console.log('token valide');
            request = request.clone({
                setHeaders: {
                    Authorization : 'Bearer ' + token,
                }
            });
        } else {
            console.log('token non valide');
            localStorage.removeItem('token');
            request.clone({
                setHeaders: {
                    Authorization : undefined
                }
            });
            // console.log(request);
        }

        if ( !request.headers.has('Content-Type')) {
            request = request.clone({
                setHeaders: {
                    'content-type': 'application/json'
                }
            });
        }
        // console.log(request);

        request = request.clone({
            headers : request.headers.set('Accept', 'application/json')
        });

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // console.log('event---->>>>', event);
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                if (error.status === 401) {
                    this.router.navigate(['login']);
                }
                if (error.status === 400) {
                    alert(error.error);
                }
                return throwError(error);
            })
        );
    }

}
