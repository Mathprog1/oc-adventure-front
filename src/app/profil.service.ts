import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { NgForm, FormGroup } from '@angular/forms';
import { Commande } from './cart/commande';

const apiUrl = 'http://192.168.99.100:8080/';
const userMSUrl = 'user/api/v1/ms/user/';
const addressMSUrl = 'address/api/v1/ms/address/';
const consistencyMSUrl = 'consistency/api/v1/ms/consistency/user/';
const commandeMSUrl = 'commande/api/v1/ms/commande/';
@Injectable({
  providedIn: 'root'
})
export class ProfilService {

  constructor(private http: HttpClient, private jwtHelperService: JwtHelperService, private router: Router,
              private authService: AuthService) { }

  getUserProfile(): Observable<any> {
    const decodeToken = this.authService.decodeToken();
    if (!decodeToken) {
      console.log('Invalide token !');
      this.router.navigate(['login']);
    }

    return this.http.get<any>(apiUrl + userMSUrl + decodeToken.externalId)
    .pipe();
  }

  editProfile(data: NgForm): Observable<any> {
    const decodeToken = this.authService.decodeToken();
    if (!decodeToken) {
      console.log('Invalide token !');
      this.router.navigate(['login']);
    }
    // return this.http.put<any> (apiUrl + userMSUrl + 'update/' + decodeToken.externalId, data).pipe();
    return this.http.put<any> (apiUrl + consistencyMSUrl + 'update/' + decodeToken.externalId, data).pipe();
  }

  updateAddress(form: any, externalId: string): Observable<any> {
    const decodeToken = this.authService.decodeToken();
    if (!decodeToken) {
      console.log('Invalide token !');
      this.router.navigate(['login']);
    }
    form.userExternalId = decodeToken.externalId;
    return this.http.put<any> (apiUrl + addressMSUrl + 'update/' + externalId, form) // go your own wayyy
    .pipe();
  }

  getUserAddressList(): Observable<any> {
    const decodeToken = this.authService.decodeToken();
    if (!decodeToken) {
      console.log('Invalide token !');
      this.router.navigate(['login']);
    }

    return this.http.get<any>(apiUrl + addressMSUrl + 'user/' + decodeToken.externalId)
    .pipe();
  }

  getUserCommandeList(): Observable<any> {
    const decodeToken = this.authService.decodeToken();
    if (!decodeToken) {
      console.log('Invalide token !');
      this.router.navigate(['login']);
    }

    return this.http.get<any>(apiUrl + commandeMSUrl + 'user/' + decodeToken.externalId)
    .pipe();
  }

  getTypeAddressList(): Observable<any> {
    return this.http.get<any>(apiUrl + addressMSUrl + 'typeaddress/?pageNb=1&nbPerPage=3')
    .pipe();
  }

  registerAddress(form: any): Observable<any> {
    const decodeToken = this.authService.decodeToken();
    if (!decodeToken) {
      console.log('Invalide token !');
      this.router.navigate(['login']);
    }

    form.userExternalId = decodeToken.externalId;
    console.log(form);
    return this.http.post<any> (apiUrl + addressMSUrl + 'register/', form)
    .pipe();
  }

  getAddressInfo(externalId: string): Observable<any> {
    return this.http.get<any>(apiUrl + addressMSUrl + externalId).pipe();
  }

getUserFacturationAddress(): Observable<any> {
  const decodeToken = this.authService.decodeToken();
  if (!decodeToken) {
      console.log('Invalide token !');
      this.router.navigate(['login']);
    }

  const userExternalId = decodeToken.externalId;

  return this.http.get<any>(apiUrl + addressMSUrl + 'user/' + userExternalId + '/typeaddress/FACTURATION').pipe();
}

createCommande(commande: Commande): Observable<any> {
  const decodeToken = this.authService.decodeToken();
  if (!decodeToken) {
      console.log('Invalide token !');
      this.router.navigate(['login']);
    }
    // commandeMSUrl + 'create'
  console.log('sending to back !!!');
  console.log(commande);
  return this.http.post<any> (apiUrl + commandeMSUrl + 'create', commande)
    .pipe();
}

}
