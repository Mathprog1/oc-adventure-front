import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm, FormControl, FormGroupDirective } from '@angular/forms';
import { ProfilService } from 'src/app/profil.service';
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material/core';
import { TypeAddress } from '../entity/typeAddress';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {


  addressForm: FormGroup;
  isLoadingResults = false;
  matcher = new MyErrorStateMatcher();
  typeAddressList: TypeAddress[];

  constructor(private formBuilder: FormBuilder, private profileService: ProfilService, private router: Router) { }

  ngOnInit() {
    this.addressForm = this.formBuilder.group({
      address: [null, Validators.required],
      city: [null, Validators.required],
      region: [null, Validators.required],
      postalCode: [null, Validators.required],
      country: [null, Validators.required],
      typeAddressExternalId: [null, Validators.required],
      userExternalId: []
    });

    this.profileService.getTypeAddressList().subscribe(res => {
      this.typeAddressList = res;
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    }
    );
  }

  // convenience getter for easy access to form fields
  get f() { return this.addressForm.controls; }

  onFormSubmit(form: NgForm) {
    // stop here if form is invalid
    this.isLoadingResults = true;
    if (this.addressForm.invalid) {
      return false;
    }

    this.profileService.registerAddress(form)
      .subscribe(res => {
        this.router.navigate(['profile']);
      }, (err) => {
        this.isLoadingResults = false;
        console.log(err);
        alert(err);
      });
  }

}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
