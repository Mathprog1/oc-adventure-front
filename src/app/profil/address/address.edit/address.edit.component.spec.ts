import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Address.EditComponent } from './address.edit.component';

describe('Address.EditComponent', () => {
  let component: Address.EditComponent;
  let fixture: ComponentFixture<Address.EditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Address.EditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Address.EditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
