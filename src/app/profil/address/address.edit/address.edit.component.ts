import { Component, OnInit } from '@angular/core';
import { ProfilService } from 'src/app/profil.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Address } from '../../entity/address';
import { TypeAddress } from '../../entity/typeAddress';
import { ErrorStateMatcher } from '@angular/material';

@Component({
  selector: 'app-address.edit',
  templateUrl: './address.edit.component.html',
  styleUrls: ['./address.edit.component.scss']
})
export class AddressEditComponent implements OnInit {

  editForm: FormGroup;
  addresse: Address;
  externalId: string;
  isLoadingResults = false;
  matcher = new MyErrorStateMatcher();
  typeAddressList: TypeAddress[];

  constructor(private profileService: ProfilService, private router: Router,
              private route: ActivatedRoute, private formBuilder: FormBuilder) {

                this.route.params.subscribe( params => this.externalId = params.externalId);
   }

     // convenience getter for easy access to form fields
  get f() { return this.editForm.controls; }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      address: [null, Validators.required],
      city: [null, Validators.required],
      region: [null, Validators.required],
      postalCode: [null, Validators.required],
      country: [null, Validators.required],
      typeAddressExternalId: [null, Validators.required],
      userExternalId: []
    });
    this.getAddresseInfo(this.externalId);
    this.profileService.getTypeAddressList().subscribe(res => {
      this.typeAddressList = res;
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    }
    );
  }

  getAddresseInfo(externalId: string) {
    this.profileService.getAddressInfo(externalId).subscribe(res => {
      this.addresse = res;
      this.editForm.controls.address.setValue(this.addresse.address);
      this.editForm.controls.city.setValue(this.addresse.city);
      this.editForm.controls.region.setValue(this.addresse.region);
      this.editForm.controls.postalCode.setValue(this.addresse.postalCode);
      this.editForm.controls.country.setValue(this.addresse.country);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    }
    );


  }


  onFormSubmit(form: NgForm) {
    // stop here if form is invalid
    this.isLoadingResults = true;
    if (this.editForm.invalid) {
      return false;
    }

    this.profileService.updateAddress(form, this.externalId)
      .subscribe(res => {
        this.router.navigate(['profile']);
      }, (err) => {
        this.isLoadingResults = false;
        console.log(err);
        alert(err);
      });
  }

}



export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
