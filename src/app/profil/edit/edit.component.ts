import { Component, OnInit } from '@angular/core';
import { ProfilService } from 'src/app/profil.service';
import { Profil } from '../entity/profil';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { MyErrorStateMatcher } from 'src/app/auth/login/login.component';
import { AuthService } from 'src/app/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  editForm: FormGroup;
  profile: Profil;
  isLoadingResults = true;
  matcher = new MyErrorStateMatcher();

  constructor(private profileService: ProfilService, private formBuilder: FormBuilder,
              private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.isLoadingResults = true;
    this.editForm = this.formBuilder.group({
      firstName: [null, Validators.required],
      mail: [null, Validators.required],
      lastName: [null, Validators.required],
      phoneNumber: [null, Validators.required]
    });
    this.getProfileInfo();
  }

  getProfileInfo(): void {
    this.profileService.getUserProfile()
    .subscribe(profileExtracted => {
      this.profile = profileExtracted;
      this.editForm.controls.firstName.setValue(this.profile.firstName);
      this.editForm.controls.lastName.setValue(this.profile.lastName);
      this.editForm.controls.mail.setValue(this.profile.mail);
      this.editForm.controls.phoneNumber.setValue(this.profile.phoneNumber);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

  onFormSubmit(form: NgForm) {
    // stop here if form is invalid
    this.isLoadingResults = true;
    if (this.editForm.invalid) {
      return;
    }
    this.profileService.editProfile(form)
      .subscribe(res => {
        this.router.navigate(['profile']);
      }, (err) => {
        console.log(err);
        alert(err);
      });
  }


}
