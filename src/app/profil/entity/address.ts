import { TypeAddress } from './typeAddress';

export class Address {
    externalId: string;
    address: string;
    city: string;
    region: string;
    postalCode: string;
    country: string;
    typeAddress: TypeAddress;
}
