export class Profil {
    firstName: string;
    mail: string;
    lastName: string;
    phoneNumber: string;
    isValid: boolean;
}
