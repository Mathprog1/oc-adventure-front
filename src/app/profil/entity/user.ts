import { Address } from './address';

export class User {
    externalId: string;
    addressList: Address[];
}
