import { Component, OnInit } from '@angular/core';
import { ProfilService } from '../profil.service';
import { Router } from '@angular/router';
import { Profil } from './entity/profil';
import { User } from './entity/user';
import { Commande } from '../cart/commande';
import {MatExpansionModule} from '@angular/material/expansion';
@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  profile: Profil;
  user: User;
  commandes;
  isLoadingProfileResults = true;
  isLoadingAddressListResults = true;
  isLoadingCommandeListResults = true;
  displayedColumns: string[] = ['position', 'name', 'weight', 'start', 'end'];
  constructor(private profileService: ProfilService, private router: Router) { }

  ngOnInit() {
    this.getProfileInfo();
    this.getAddressInfo();
    this.getCommandesInfos();
  }

  getAddressInfo() {
        this.profileService.getUserAddressList()
    .subscribe(userExtracted => {
      this.user = userExtracted;
      console.log(this.user);
      this.isLoadingAddressListResults = false;
    }, err => {
      console.log(err);
      this.isLoadingAddressListResults = false;
    });
  }

  getProfileInfo(): void {
    this.profileService.getUserProfile()
    .subscribe(profileExtracted => {
      this.profile = profileExtracted;
      console.log(this.profile);
      this.isLoadingProfileResults = false;
    }, err => {
      console.log(err);
      this.isLoadingProfileResults = false;
    });

  }

  getCommandesInfos(): void {
    this.profileService.getUserCommandeList()
    .subscribe(CommandeExtracted => {
      this.commandes = CommandeExtracted;
      console.log(this.commandes);
      this.isLoadingCommandeListResults = false;
    }, err => {
      console.log(err);
      this.isLoadingCommandeListResults = false;
    });
  }

}
