import { Component, OnInit, Injectable, Input } from '@angular/core';
import { Session } from './session';
import { Router,ActivatedRoute } from '@angular/router';
import { SessionService } from '../../_service/session.service';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})

@Injectable({
  providedIn: 'root'
})
export class SessionComponent implements OnInit {

  sessions = [];

  selectedSession : Session;
  pageNb = 1;
  nbPerPage = 30;
  displayedColumns: string[] = ['id','startDate','endDate','limitBookingDate','price','totalAvailablePlace','button'];
  isLoadingResults = true;
  selectSession = false;
  sub =    this.route.params.subscribe(params => {
    this.adventureExternalId = params ['externalId'];
    this.adventureName = params ['nameAdventure'];
  });
 adventureExternalId: string;
 adventureName: string;

  constructor(private router: Router, private sessionService: SessionService,private route: ActivatedRoute) { }
 

  getSessions(): void {
    this.sessionService.getSessions(this.pageNb,this.nbPerPage)
    .subscribe(session => {
      this.sessions = session;
      console.log("data : ",this.sessions);
      this.isLoadingResults = false;
    }, err => {
      console.log(err),
      this.isLoadingResults = false;
    })
  }

  getSessionsByAdventureId(): void {
    let s = [];
    this.sessionService.getSessionsByAdventureId(this.adventureExternalId,this.pageNb,this.nbPerPage)
    .subscribe(sessionsFromDB => {
      sessionsFromDB.forEach(item => 
        s.push(new Session(item))
      );
      this.sessions = s;
      this.sessions.forEach(item => item["name"]= this.adventureName);
      console.log("data : ",this.sessions);
      this.isLoadingResults = false;
    }, err => {
      console.log(err),
      this.isLoadingResults = false;
    })
  }

ngOnInit() {
  this.getSessionsByAdventureId();
}

onSelect(s: Session):void {
  if(this.selectSession===false) {
    this.selectedSession = s;
    this.selectSession = true;
  }else{
    this.selectedSession = null;
    this.selectSession = false;
  }
}
}


