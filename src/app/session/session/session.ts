import { SessionCartItem } from '../../cart/sessionCartItem';

export class Session extends SessionCartItem{

    constructor(itemData: any){
        super(itemData);
        this.limitBookingDate = itemData.limitBookingDate;
        this.totalAvailablePlace = itemData.totalAvailablePlace;
        this.description = itemData.description;
    }

	public limitBookingDate: Date;
	public totalAvailablePlace: number;
	public description: string;
}